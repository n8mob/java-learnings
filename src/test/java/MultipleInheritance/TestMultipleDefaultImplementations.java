package MultipleInheritance;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestMultipleDefaultImplementations {
    @Test
    void testMultipleInterfacesWithDefaultImplementations() {
        MultipleInterfaces unitUnderTest = new MultipleInterfaces();

        var actual = unitUnderTest.doTheThing();

        assertTrue(actual.contains("did the thing"), "do the thing");
        assertTrue(actual.contains("Interface 1"), "Interface 1");
        assertTrue(actual.contains("Interface 2"), "Interface 2");
        assertTrue(actual.contains("conflicting defaults"), "conflicting defaults");
    }
}
