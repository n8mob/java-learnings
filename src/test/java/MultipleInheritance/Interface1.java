package MultipleInheritance;

public interface Interface1 {
    default String doTheThing() {
        return "Interface 1 did the thing";
    }
}
