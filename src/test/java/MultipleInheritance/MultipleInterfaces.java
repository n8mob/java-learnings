package MultipleInheritance;

public class MultipleInterfaces implements Interface1, Interface2 {
    @Override
    public String doTheThing() {
        var s1 = Interface1.super.doTheThing();
        var s2 = Interface2.super.doTheThing();

        return "Had to override because of conflicting defaults.\n\t" + s1 + "\n\t" + s2;
    }
}
