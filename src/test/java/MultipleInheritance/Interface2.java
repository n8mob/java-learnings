package MultipleInheritance;

public interface Interface2 {
    default String doTheThing() {
        return "Interface 2 did the thing";
    }
}
