import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JavaTime {
    @Test
    void testDurationWithDifferentUnits() {
        var threeHours = Duration.ofHours(3);
        var oneHundredEightyMinutes = Duration.ofMinutes(180);

        assertEquals(threeHours, oneHundredEightyMinutes);
    }

    @Test
    void testDurationStrings() {
        var oneHundredSeventyMinutes = Duration.ofMinutes(170);

        var actual = oneHundredSeventyMinutes.toString();
        assertTrue(actual.contains("PT"), "why does it start with \"PT\"?");
        assertTrue(actual.contains("2H"), "170 minutes is longer than two hours");
        assertTrue(actual.contains("50M"), "170 minutes should have a minutes component");
    }

    @Test
    void testDivideByMinutes() {
        var threeHours = Duration.ofHours(3);
        var twentyMinutes = Duration.ofMinutes(20);

        var actual = threeHours.dividedBy(twentyMinutes);

        assertEquals(9, actual);
    }

    @Test
    void testUnevenUnitOfMinutes() {
        var moreThanThreeHours = Duration.ofMinutes(190);
        var twentyMinutes = Duration.ofMinutes(20);

        var actual = moreThanThreeHours.dividedBy(twentyMinutes);

        assertNotEquals(10, actual);
    }

    @Test
    void testDivideByCount() {
        var moreThanThreeHours = Duration.ofMinutes(190);
        var twentyOneMinutes = Duration.ofMinutes(21);
        var segments = 9;

        var actual = moreThanThreeHours.dividedBy(segments);

        assertTrue(actual.compareTo(twentyOneMinutes) > 0);
    }

    @Test
    void testMultiplyingDurations() {
        var moreThanThreeHours = Duration.ofMinutes(190);
        var twentyMinutes = Duration.ofMinutes(20);
        var segments = moreThanThreeHours.dividedBy(twentyMinutes);

        var maxMultipleOfTwenty = twentyMinutes.multipliedBy(segments);

        assertTrue(maxMultipleOfTwenty.compareTo(moreThanThreeHours) < 0);

        var remainder = moreThanThreeHours.minus(maxMultipleOfTwenty);
        var tenMinutes = Duration.ofMinutes(10);

        assertEquals(tenMinutes, remainder);
    }

    @Test
    void testNoRemainderGiveZeroDuration() {
        var threeHours = Duration.ofHours(3);
        var twentyMinutes = Duration.ofMinutes(20);
        var segments = threeHours.dividedBy(twentyMinutes);
        var remainder = threeHours.minus(twentyMinutes.multipliedBy(segments));

        final Duration noTimeAtAll = Duration.ofNanos(0);

        assertEquals(noTimeAtAll, remainder);
    }
}
