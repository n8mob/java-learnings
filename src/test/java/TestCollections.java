import org.junit.jupiter.api.Test;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCollections {
    @Test
    void canCollectStringFromTwoCollectorsAndMergeResult() {
        SalaryRange salaryRange = Stream
                .of(56700, 67600, 45200, 120000, 77600, 85000)
                .collect(teeing(
                        minBy(Integer::compareTo),
                        maxBy(Integer::compareTo),
                        SalaryRange::fromOptionalIntegers
                ));

        assertEquals("SalaryRange{min=45200, max=120000}", salaryRange.toString());
    }

    @Test
    void howToCollectAStream()
    {
        var someIntegers = IntStream.range(0, 25)
                .boxed()
                .collect(Collectors.toList());

        assertEquals(25, someIntegers.size());
    }
}

