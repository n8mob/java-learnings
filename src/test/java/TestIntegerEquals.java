import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestIntegerEquals {

    @SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
    @Test
    void testPrimitiveIntsCompareCorrectly() {
        assertTrue(Integer.MAX_VALUE == 2_147_483_647, "equal literal and constant with `==`");
        assertTrue(2_147_483_647 == 2_147_483_647, "equal literals with `==`");
        assertEquals(2_147_483_647, 2_147_483_647, "equal literals with `assertEquals(...)`");
        assertNotEquals(2_147_483_647, 2_147_483_646, "unequal literals with `assertNotEquals(...)`");
        assertFalse(2_147_483_647 == 2_147_483_646, "unequal literals with `==`");
        assertNotEquals(2_147_483_648L, 12_147_483_647L, "Unequal long literals with `assertNotEquals(...)`");
        assertFalse(2_147_483_648L == 12_147_483_647L, "unequal long literals with `==`");
    }

    @SuppressWarnings({
        "WrapperTypeMayBePrimitive",
        "NumberEquality",
        "SimplifiableAssertion",
        "ConstantConditions"
    })
    @Test
    void testIntegersCompareCorrectly() {
        final Integer intMaxConstant = Integer.MAX_VALUE;
        final Integer intMaxLiteral = 2_147_483_647;
        final Integer intMaxMinusOne = 2_147_483_646;
        final Long intMaxPlusOne = 2_147_483_648L;
        final long intMaxPlusTenBillion = 12_147_483_647L;

        assertFalse(intMaxConstant == intMaxLiteral, "equal Integers with `==`");
        assertEquals(intMaxLiteral, intMaxLiteral, "equal Integers with `assertEquals(...)`");
        assertNotEquals(intMaxLiteral, intMaxMinusOne, "unequal Integers with `assertEquals(...)`");
        assertFalse(intMaxLiteral == intMaxMinusOne, "unequal Integers with `==`");
        assertNotEquals(intMaxPlusOne, intMaxPlusTenBillion, "unequal Longs with `assertEquals(...)`");
        assertFalse(intMaxPlusOne == intMaxPlusTenBillion, "unequal Longs with `==`");
    }
}
