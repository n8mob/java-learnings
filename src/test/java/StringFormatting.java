import org.junit.jupiter.api.Test;

import java.util.MissingFormatArgumentException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class StringFormatting {
    @Test
    void testWrongNumberOfStringFormatArguments() {
        String format = "%s, %s";

        String actual1 = format.formatted(1, 2, 3);

        assertEquals("1, 2", actual1);

        try {
            String actual2 = format.formatted(4);
        } catch (RuntimeException rex) {
            assertTrue(rex instanceof MissingFormatArgumentException);
        }

    }
}
