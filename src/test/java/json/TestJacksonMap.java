package json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestJacksonMap {
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void testMapAsExpected() throws JsonProcessingException {
        var aMap = Map.of(
            "1", "one",
            "2", "two"
        );

        var actual = objectMapper.writeValueAsString(aMap);

        assertThat(actual, startsWith("{"));
        assertThat(actual, endsWith("}"));

        assertThat(actual, containsString("\"1\":\"one\""));
        assertThat(actual, containsString("\"2\":\"two\""));
    }

    @Test
    void testImplementsMap() throws JsonProcessingException {
        class myMap implements Map<String, String> {
            @Override
            public int size() {
                return _map.size();
            }

            @Override
            public boolean isEmpty() {
                return _map.isEmpty();
            }

            @Override
            public boolean containsKey(Object key) {
                return _map.containsKey(key);
            }

            @Override
            public boolean containsValue(Object value) {
                return _map.containsValue(value);
            }

            @Override
            public String get(Object key) {
                return _map.get(key);
            }

            @Override
            public String put(String key, String value) {
                return _map.put(key, value);
            }

            @Override
            public String remove(Object key) {
                return _map.remove(key);
            }

            @Override
            public void putAll(Map<? extends String, ? extends String> m) {
                _map.putAll(m);
            }

            @Override
            public void clear() {
                _map.clear();
            }

            @Override
            public Set<String> keySet() {
                return _map.keySet();
            }

            @Override
            public Collection<String> values() {
                return _map.values();
            }

            @Override
            public Set<Entry<String, String>> entrySet() {
                return _map.entrySet();
            }

            private final Map<String, String> _map;

            myMap() {
                _map = new HashMap<>();
            }
        }

        var aMap = new myMap();
        aMap.put("1", "one");
        aMap.put("2", "two");

        var actual = objectMapper.writeValueAsString(aMap);

        assertThat(actual, containsString("\"1\":\"one\""));
        assertThat(actual, containsString("\"2\":\"two\""));
    }
}
