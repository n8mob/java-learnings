import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class InstantsAndTimestamps {
    @Test
    void testTimestampFromNow() {
        final Timestamp actual = Timestamp.from(Instant.now());
        assertNotNull(actual);
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
    @Test
    void testTimestampFromNull() {
        assertThrows(NullPointerException.class, () -> Timestamp.from(null));
    }
}
