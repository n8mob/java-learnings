import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestOverflowingSeconds {
    @Test
    void testHowManySeconds() {
        final Duration maxSeconds = Duration.ofSeconds(Integer.MAX_VALUE);
        final Duration aYear = Duration.ofDays(365);
        final long maxSecondsOfDays = maxSeconds.toDays();
        assertTrue(maxSecondsOfDays > aYear.toDays(), "two billion seconds should be more than a year");
        assertTrue(maxSecondsOfDays > 730, "two billion seconds should be more than two years");
        assertTrue(maxSecondsOfDays > 3650, "ten years?");
        assertFalse(maxSecondsOfDays > 36_500, "one hundred years? (" + maxSeconds.toDaysPart() + " days)");
    }
}
