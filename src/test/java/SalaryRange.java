import java.util.Optional;

public class SalaryRange {
    private final Integer min;
    private final Integer max;

    private SalaryRange(Integer min, Integer max) {
        this.min = min;
        this.max = max;
    }

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    public static SalaryRange fromOptionalIntegers(Optional<Integer> min, Optional<Integer> max) {
        if (min.isEmpty() || max.isEmpty()) {
            throw new IllegalStateException("Minimum and maximum salaries cannot be null.");
        }
        return new SalaryRange(min.get(), max.get());
    }

    @Override
    public String toString() {
        return "SalaryRange{" +
                "min=" + min +
                ", max=" + max +
                '}';
    }
}
