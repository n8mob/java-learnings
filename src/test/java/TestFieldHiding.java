import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFieldHiding {

  private static final String PARENT_STRING = "Parent's string";
  private static final String CHILD_STRING = "Child's string";

  public static class Parent {
    public String aField = PARENT_STRING;

    public String getAProperty() {
      return this.aField;
    }
  }

  public static class Child extends Parent {
    public String aField = CHILD_STRING;

    @Override
    public String getAProperty() {
      return this.aField;
    }
  }

  Child child = new Child();
  Parent parent = new Parent();

  @Test
  void testChildField() {
      assertEquals(CHILD_STRING, child.aField);
  }

  @Test
  void testParentField() {
      assertEquals(PARENT_STRING, parent.aField);
  }

  @Test
  void testDownCast() {
      Parent parent2 = child;
      assertEquals(PARENT_STRING, parent2.aField);
  }

  @Test
  void testMethodDispatch() {
    Parent parent3 = child;
    assertEquals(CHILD_STRING, parent3.getAProperty());
  }
}
