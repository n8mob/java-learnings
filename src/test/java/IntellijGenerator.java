public class IntellijGenerator {
    private final String foo;
    private final int bar;
    private final float rootBeer;
    private final String message;

    public IntellijGenerator(String foo, int bar, float rootBeer, String message) {
        this.foo = foo;
        this.bar = bar;
        this.rootBeer = rootBeer;
        this.message = message;
    }
}
