import java.util.UUID;

public class UuidGenerator {
    public static void main(String[] args) {
        int n;
        if (args.length < 1) {
            n = 15;
        } else {
            n = Integer.parseInt(args[0]);
        }

        for (int i = 0; i < n; i++) {
            System.out.printf("'%s'%n", UUID.randomUUID());
        }
    }
}
