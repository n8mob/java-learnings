import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestEnumFromString {
    private enum AnEnum {
        ONE("a-whone"),
        TWO("a-twhoo"),
        THREE("thre-hee");

        private final String fancyName;

        AnEnum(String fancyName) {
            this.fancyName = fancyName;
        }

        public String getName() {
            return this.name().toLowerCase(Locale.ROOT);
        }

        public String getFancyName() {
            return fancyName;
        }
    }

  @Test
  void testFromString() {
        AnEnum actual = AnEnum.valueOf("TWO");

        assertEquals(AnEnum.TWO, actual);
  }

  @Test
    void testToLowerCaseString() {
        AnEnum actual = AnEnum.THREE;

        assertEquals("three", actual.getName());
  }

  @Test
    void testFancyName() {
        AnEnum actual = AnEnum.THREE;

        assertEquals("thre-hee", actual.getFancyName());
  }

  @Test
    void testToInt() {
        assertEquals(0, AnEnum.ONE.ordinal());
  }

  @Test
  void testFromInt() {
        assertEquals(AnEnum.ONE, AnEnum.values()[0]);
  }

  @Test
    void testToString() {
        String expected = "ONE";
        String actual = AnEnum.ONE.toString();
        assertEquals(expected, actual);
  }
}
