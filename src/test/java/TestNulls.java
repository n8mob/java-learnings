import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

@SuppressWarnings({"SimplifiableAssertion", "ConstantConditions"})
public class TestNulls {
    @Test
    void testThatNullEvaluatesToFalse() {
        Boolean wrappedNull = null;

        assertFalse(Boolean.TRUE.equals(wrappedNull));
    }

    @Test
    void testThatNullThrows() {
        Boolean wrappedNull = null;

        try {
            if (!wrappedNull) {
                fail("if null should throw");
            }
        } catch (NullPointerException npex) {
            assertNotNull(npex.getMessage());
        }
    }

    @Test
    void testUnwrappingNull() {
        assertFalse(returnsBooleanFalse());

        try {
            final boolean booleanNull = returnsBooleanNull();
            assertFalse(booleanNull);
        } catch (NullPointerException npe) {
            assertNotNull(npe);
        }
    }

    boolean returnsBooleanFalse() {
        return Boolean.FALSE;
    }

    Boolean returnsBooleanNull() {
        return null;
    }
}
